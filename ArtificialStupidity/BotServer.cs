﻿using ArtificialStupidity.Messages;
using ArtificialStupidity.Messages.Client;

using ArtificialStupidity.Messages.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity {
    class BotServer {
        private readonly TcpClient client;
        private readonly StreamReader reader;
        private readonly StreamWriter writer;

        public event Action<BaseMessage> GotMessage;

        private readonly MessageConverter messageConverter;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        private bool running = false;

        public BotServer() {
            this.client = new TcpClient(Settings.HOST, Settings.PORT);
            NetworkStream stream = this.client.GetStream();
            this.reader = new StreamReader(stream);
            this.writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            jsonSerializerSettings = new JsonSerializerSettings() {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            messageConverter = new MessageConverter();
        }

        public void Start(BaseMessage joinMessage) {
            running = true;
            Send(joinMessage);
            start();
        }

        private void start() {
            string data;
            while (running && (data = reader.ReadLine()) != null) {
                //Console.WriteLine("<-" + data);
                BaseMessage msg = JsonConvert.DeserializeObject<BaseMessage>(data, messageConverter);

                //Console.WriteLine("msg {0} {1}", msg.MsgType, msg.GetType());

                if (GotMessage != null)
                    GotMessage(msg);
            }
        }

        public void Send(BaseMessage msg) {
            string messageString = JsonConvert.SerializeObject(msg, jsonSerializerSettings);
            
            writer.WriteLine(messageString);
        }

        public void Stop() {
            running = false;
        }
    }

    class MessageConverter : JsonConverter {

        public override bool CanConvert(Type objectType) {
            return objectType.IsAssignableFrom(typeof(BaseMessage));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            JObject jsonObject = JObject.Load(reader);
            string msgType = jsonObject.Property("msgType").Value.ToString();

            if (msgType == @"yourCar")
                return jsonObject.ToObject<YourCar>();
            else if (msgType == @"gameInit")
                return jsonObject.ToObject<GameInit>();
            else if (msgType == @"gameStart")
                return jsonObject.ToObject<GameStart>();
            else if (msgType == @"carPositions")
                return jsonObject.ToObject<CarPositions>();
            else if (msgType == @"gameEnd")
                return jsonObject.ToObject<GameEnd>();
            else if (msgType == @"tournamentEnd")
                return jsonObject.ToObject<TournamentEnd>();
            else if (msgType == @"crash")
                return jsonObject.ToObject<Crash>();
            else if (msgType == @"spawn")
                return jsonObject.ToObject<Spawn>();
            else if (msgType == @"lapFinished")
                return jsonObject.ToObject<LapFinished>();
            else if (msgType == @"dnf")
                return jsonObject.ToObject<Dnf>();
            else if (msgType == @"finish")
                return jsonObject.ToObject<Finish>();
            else if (msgType == @"error")
                return jsonObject.ToObject<Error>();
            else if (msgType == @"join")
                return jsonObject.ToObject<JoinMessage>();
            else if (msgType == @"turboAvailable")
                return jsonObject.ToObject<TurboAvailable>();
            else if (msgType == @"turboStart")
                return jsonObject.ToObject<TurboStart>();
            else if (msgType == @"turboEnd")
                return jsonObject.ToObject<TurboEnd>();
            else {
                Console.WriteLine("unhandled message--------------\n{0}\n--------", jsonObject.ToString());
                return jsonObject.ToObject<BaseMessage>();
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            throw new NotImplementedException(@"Read only serializer!");
        }

        public override bool CanRead { get { return true; } }
        public override bool CanWrite { get { return false; } }
    }
}
