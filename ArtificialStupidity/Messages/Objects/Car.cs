﻿using ArtificialStupidity.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Car {
        public CarId Id;
        public Dimensions Dimensions;
    }
}
