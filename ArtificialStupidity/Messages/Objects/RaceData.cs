﻿using ArtificialStupidity.Messages.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class RaceData {
        public RaceData() {
        }

        public RaceData(string trackName) {
            BotId = new Join() { Name = Settings.BOTNAME, Key = Settings.BOTKEY };
            Password = "";
            CarCount = 1;
            TrackName = trackName;
        }

        public Join BotId;
        public String TrackName;
        public String Password = "";
        public int CarCount = 1;
    }
}
