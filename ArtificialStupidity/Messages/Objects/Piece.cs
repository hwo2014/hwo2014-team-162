﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Piece {
        public Piece() {
            Angle = 0f;
            Radius = 0f;
            Switch = false;
        }

        public float Length;
        public bool Switch;
        public float Angle;
        public float Radius;
    }
}
