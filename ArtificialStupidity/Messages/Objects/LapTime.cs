﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class LapTime {
        public int Lap;
        public long Ticks;
        public long Millis;
    }
}
