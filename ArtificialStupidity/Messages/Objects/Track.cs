﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Track {
        public string Id;
        public string Name;

        public List<Piece> Pieces;
        public List<Lane> Lanes;
        public Point StartingPoint;
    }
}
