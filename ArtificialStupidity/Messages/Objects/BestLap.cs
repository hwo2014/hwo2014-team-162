﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class BestLap {
        public CarId Car;
        public LapTime Result;
    }
}
