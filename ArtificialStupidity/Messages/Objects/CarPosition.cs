﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class CarPosition {
        public CarId Id;
        public float Angle;
        public PiecePosition PiecePosition;
        public int Lap;
    }
}
