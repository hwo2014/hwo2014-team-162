﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity.Messages.Objects {
    class TurboData {
        public float TurboDurationMilliseconds;
        public float TurboDurationTicks;
        public float TurboFactor;
    }
}
