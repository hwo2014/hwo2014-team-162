﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class PiecePosition {
        public int PieceIndex;
        public float InPieceDistance;
        public LanePosition Lane;
    }
}
