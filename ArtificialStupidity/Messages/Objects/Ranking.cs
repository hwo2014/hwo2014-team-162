﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Ranking {
        public int Overall;
        public int FastestLap;
    }
}
