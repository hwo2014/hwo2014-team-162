﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity.Messages.Objects {
    class GameEndData {
        public List<RaceResult> Results;
        public List<BestLap> BestLaps;
    }
}
