﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Dimensions {
        public float Length;
        public float Width;
        public float GuideFlagPosition;
    }
}
