﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity.Messages.Objects {
    class LanePosition {
        public int StartLaneIndex;
        public int EndLaneIndex;
    }
}
