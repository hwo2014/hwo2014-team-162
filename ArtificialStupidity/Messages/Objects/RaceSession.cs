﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class RaceSession {
        public int Laps;
        public int MaxLapTimeMs;
        public bool QuickRace;
        public long DurationMs;
    }
}
