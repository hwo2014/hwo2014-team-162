﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class RaceTime {
        public int Laps;
        public long Ticks;
        public long Millis;
    }
}
