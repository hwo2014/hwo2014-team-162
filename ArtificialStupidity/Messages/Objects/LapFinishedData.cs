﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class LapFinishedData {
        public CarId Car;
        public LapTime LapTime;
        public RaceTime RaceTime;
        public Ranking Ranking;
    }
}
