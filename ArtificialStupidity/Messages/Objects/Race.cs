﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class Race {
        public Track Track;
        public List<Car> Cars;
        public RaceSession RaceSession;
    }
}
