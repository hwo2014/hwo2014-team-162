﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Objects {
    class RaceResult {
        public CarId Car;
        public RaceTime Result;
    }
}
