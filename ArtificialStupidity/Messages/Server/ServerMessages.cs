﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ArtificialStupidity.Messages.Objects;

namespace ArtificialStupidity.Messages.Server {
    class YourCar : DataMessage<CarId> { }
    class GameInit : DataMessage<GameInitData> { }
    class GameStart : DataMessage<dynamic> { }
    class CarPositions : GameMessage<List<CarPosition>> { }
    class GameEnd : DataMessage<GameEndData> { }
    class TournamentEnd : DataMessage<dynamic> { }
    class Crash : GameMessage<CarId> { }
    class Spawn : GameMessage<CarId> { }
    class LapFinished : GameMessage<LapFinishedData> { }
    class Dnf : GameMessage<DnfData> { }
    class Finish : GameMessage<CarId> { }
    class Error : DataMessage<string> { }
    class TurboAvailable : DataMessage<TurboData> { }
    class TurboStart : GameMessage<CarId> { }
    class TurboEnd : GameMessage<CarId> { }
}
