﻿using ArtificialStupidity.Messages.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Client {
    class JoinMessage : DataMessage<Join> {
        public JoinMessage() { }
        public JoinMessage(string botkey, string botname) {
            MsgType = "join";
            this.Data = new Join() { Name = botname, Key = botkey };
        }
    }
}
