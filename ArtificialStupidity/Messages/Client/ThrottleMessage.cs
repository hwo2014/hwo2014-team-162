﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Client {
    class ThrottleMessage : TickMessage<float> {
        public ThrottleMessage(float throttle, long tick) {
            this.MsgType = "throttle";

            this.Data = throttle;

            this.GameTick = tick;
        }
    }
}
