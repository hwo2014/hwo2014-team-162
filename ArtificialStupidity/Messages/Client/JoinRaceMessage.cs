﻿using ArtificialStupidity.Messages.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity.Messages.Client {
    class JoinRaceMessage : DataMessage<RaceData> {
        public JoinRaceMessage() {
            MsgType = "joinRace";
        }
    }
}
