﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Client {
    class SwitchLaneMessage : DataMessage<string> {
        public enum Direction {
            Left,
            Right,
            Same
        }

        public SwitchLaneMessage(Direction direction) {
            MsgType = "switchLane";
            Data = direction.ToString();
        }
    }
}
