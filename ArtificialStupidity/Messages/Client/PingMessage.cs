﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages.Client {
    class PingMessage : BaseMessage {
        public PingMessage() {
            MsgType = "ping";
        }
    }
}
