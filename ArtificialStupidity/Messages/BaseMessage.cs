﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity.Messages {
    class BaseMessage {
        public string MsgType;
    }

    class DataMessage<T> : BaseMessage {
        public T Data;
    }

    interface ITickMessage {
        long GameTick { get; }
    }

    class TickMessage<T> : DataMessage<T>, ITickMessage {
        public long GameTick { get; set; }
    }

    interface IGameMessage {
        string GameId { get; }
    }

    class GameMessage<T> : TickMessage<T>, IGameMessage {
        public string GameId { get; set; }
    }
}
