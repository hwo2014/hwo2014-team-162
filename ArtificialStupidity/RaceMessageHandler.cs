﻿using ArtificialStupidity.Messages;
using ArtificialStupidity.Messages.Client;
using ArtificialStupidity.Messages.Objects;
using ArtificialStupidity.Messages.Server;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtificialStupidity {
    class RaceMessageHandler {
        public RaceInformation RaceInfo = new RaceInformation();

        public BaseMessage React(BaseMessage currentMessage) {
            //Console.WriteLine("GAME TICK: " + currentMessage.GameTick);
            
            if (currentMessage is ITickMessage) {
                ITickMessage tickMessage = currentMessage as ITickMessage;
                if (tickMessage.GameTick > RaceInfo.GameTick)
                    RaceInfo.GameTick = tickMessage.GameTick;
            }


            if (currentMessage is CarPositions) {
                CarPositions carPosition = currentMessage as CarPositions;
                RaceInfo.UpdateRaceStatus(carPosition.Data);
                return RaceInfo.GetResponseMessage();
            } else if (currentMessage is GameInit) {
                GameInit gameInit = currentMessage as GameInit;
                this.RaceInfo.CurrentRace = gameInit.Data.Race;
                Console.WriteLine("Race:  {0}", gameInit.Data.Race.Track.Name);
                return new EmptyMessage();
            } else if (currentMessage is YourCar) {
                YourCar yourCar = currentMessage as YourCar;
                this.RaceInfo.MyCar = yourCar.Data;
                Console.WriteLine("my car is " + yourCar.Data.Color);
                return new EmptyMessage();
            } else if (currentMessage is GameEnd) {
                GameEndData data = (currentMessage as GameEnd).Data;
                RaceInfo.GameTick = -1;
                RaceInfo.opponents = null;
                Console.WriteLine("Race finished");
                Console.WriteLine("Results");
                Console.WriteLine("--------------------------");
                foreach (var result in data.Results) {
                    Console.WriteLine(result.Car.Name + " : " + ((float) result.Result.Millis) / 1000f);
                }
                return new EmptyMessage();
            } else if (currentMessage is GameStart) {
                Console.WriteLine("Race starts");
                return new EmptyMessage();
            } else if (currentMessage is Crash) {
                Crash crash = currentMessage as Crash;
                Console.WriteLine(crash.Data.Name + " crashed");
                RaceInfo.Speed = 0f;
                return new EmptyMessage();
            } else if (currentMessage is Spawn) {
                Spawn spawn = currentMessage as Spawn;
                Console.WriteLine(spawn.Data.Name + " spawned");
                RaceInfo.Speed = 0f;
                return new EmptyMessage();
            } else if (currentMessage is LapFinished) {
                LapFinishedData lapFinishedData = (currentMessage as LapFinished).Data;
                Console.WriteLine("Lap: " + lapFinishedData.Car.Name + " " + ((float)lapFinishedData.LapTime.Millis / 1000f));
                return new EmptyMessage();
            } else if (currentMessage is TurboAvailable) {
                TurboData turboData = (currentMessage as TurboAvailable).Data;
                Console.WriteLine("Got turbo");
                RaceInfo.Turbo = turboData;
                return new EmptyMessage();
            } else if (currentMessage is TurboStart) {
                CarId carId = (currentMessage as TurboStart).Data;
                Console.WriteLine("{0} used turbo", carId.Name);
                return new EmptyMessage();
            } else if (currentMessage is TurboEnd) {
                CarId carId = (currentMessage as TurboEnd).Data;
                Console.WriteLine("{0} ran out of turbo", carId.Name);
                return new EmptyMessage();
            } else if (currentMessage is TournamentEnd) {
                return null; 
            }

            Console.WriteLine("unhandled message: {0}", currentMessage.MsgType);
            return new EmptyMessage();
        }
    }
}
