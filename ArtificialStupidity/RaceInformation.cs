﻿using System.Security.Cryptography.X509Certificates;
using ArtificialStupidity.Messages.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtificialStupidity.Messages.Client;
using ArtificialStupidity.Messages;
using ArtificialStupidity.Messages.Server;

namespace ArtificialStupidity {
    class RaceInformation
    {
        private Race _currentRace;

        public Dictionary<int, Corner> Corners = new Dictionary<int, Corner>();
        public Race CurrentRace
        {
            get { return _currentRace;  }
            set
            {
                _currentRace = value;

                Corner currentCorner = null;

                var index = 0;
                foreach (var piece in value.Track.Pieces)
                {
                    if (!IsAnglePiece(piece))
                    {
                        if (currentCorner != null)
                        {
                            currentCorner = null;
                        }

                        index++;
                        continue;
                    }
                    if (currentCorner == null ||  Math.Abs(piece.Radius - currentCorner.BaseRadius) > 0.1f || !(piece.Angle * currentCorner.BaseAngle >= 0.0f))
                    {
                        currentCorner = new Corner() { StartIndex = index, BaseRadius = piece.Radius, BaseAngle = piece.Angle };
                    }

                    foreach (var lane in value.Track.Lanes)
                    {
                        var pp = new PiecePosition()
                        {
                            Lane = new LanePosition() {EndLaneIndex = lane.Index, StartLaneIndex = lane.Index}
                        };
                        var pieceLength = getPieceLength(piece, pp);

                        if (!currentCorner.Lanes.ContainsKey(lane.Index))
                        {
                            var radius = getCorrectedRadius(piece, pp);
                            currentCorner.Lanes[lane.Index] = new CornerLane() {LaneIndex = lane.Index, Radius = radius};
                        }
                        
                        currentCorner.Lanes[lane.Index].Length += pieceLength;
                    }

                    Corners[index] = currentCorner;

                    index++;
                }
            }
        }
        public CarId MyCar;

        public CarPosition LastPosition;
        public CarPosition CurrentPosition;
        public Piece CurrentPiece;

        public Dictionary<string, OpponentData> opponents;
        public OpponentData NearestOpponent;
        public float DistanceToNearestOpponent = 99999f;

        public TurboData Turbo;
        public bool TurboAvailable { get { return Turbo != null; } }

        private const int CALCULATION_TICK_COUNT = 4;  // Needs to be divisible by two
        
        private float throttleK;
        private float throttleA;
        private float maxSpeed;
           
        public float LastSpeed;
        public float LastSlipAngleVelocity;

        public long GameTick;
        public float Speed;
        public float DistanceTraveled;
        public float Acceleration;
        public float SlipAngleVelocity;

        private float accDistance;
        private long accTicks;
        
        public List<CarPosition> CarPositions;

        private float lastDeltaDistance;
        private float lastDeltaCarAngle;

        private bool SwitchRequested;

        private void CalculateSpeed() {
            LastSpeed = Speed;
            Speed = getDeltaDistance(LastPosition == null ? null : LastPosition.PiecePosition, CurrentPosition.PiecePosition);
            DistanceTraveled += Speed;

            LastSlipAngleVelocity = SlipAngleVelocity;

            if (LastPosition != null)
                SlipAngleVelocity = getDeltaCarAngle(LastPosition, CurrentPosition);
        }

        float throttle;
        public float GetThrottle() {
            if (!ThrottleConstantsCalculated)
                return CalculateThrottleConstants();

            var currentPieceTargetSpeed = getTargetSpeed(CurrentPosition.PiecePosition.PieceIndex);
            var nextCornerTargetSpeed = getTargetSpeed(getNextCornerPiece());

            if (Speed > nextCornerTargetSpeed && getDistanceBeforeCorner() - getVelocityChangeDistance(Speed, nextCornerTargetSpeed) < Speed) {
                throttle = 0f;
            } else if (!IsAnglePiece(CurrentPiece)) {
                throttle = 1f;
            } else if (Speed > currentPieceTargetSpeed) {
                throttle = 0f;
            }
            else if (Speed < currentPieceTargetSpeed && currentPieceTargetSpeed - Speed < 0.1f)
            {
                throttle = getTargetSpeed(CurrentPosition.PiecePosition.PieceIndex) * 0.1f;
            } else {
                throttle = 1f;
            }

            return throttle;
        }

        private float getTargetSpeed(int p) {
            var piece = CurrentRace.Track.Pieces[p];
            
            if (!IsAnglePiece(piece))
                return 10f;

            var correctedRadius = getCorrectedRadius(piece, CurrentPosition.PiecePosition);
            var baseSpeed = (float) Math.Sqrt(correctedRadius*0.305f) + 1.0f;

            var learnedSpeed = 0f;
            var laneIndex = CurrentPosition.PiecePosition.Lane.EndLaneIndex;


           if (Corners.ContainsKey(p) && Corners[p].Lanes[laneIndex].Exits.Count > 0)
            {
                var l = Corners[p].Lanes[laneIndex];
                var exits = l.Exits.OrderByDescending(o => o.Speed );
                var exit = exits.First(o => o.Angle < 56f);

                if (exit != null)
                {

                    var angle = exit.Angle;
                    var exitSpeed = exit.Speed;

                    if (angle < 20f)
                        learnedSpeed = exitSpeed + (correctedRadius / 40f) * 0.25f;
                    else if (angle < 43f)
                        learnedSpeed = exitSpeed + (correctedRadius / 60f) * 0.10f;
                    else if (angle < 48f)
                        learnedSpeed = exitSpeed + (correctedRadius / 60f) * 0.05f;
                    else if (angle < 53f)
                        learnedSpeed = exitSpeed + (correctedRadius / 60f) * 0.03f;
                    else
                        learnedSpeed = exitSpeed;
                }
            }

            if (learnedSpeed < baseSpeed)
                learnedSpeed = baseSpeed;


            var maxSpeed = (float) Math.Sqrt(0.62f*correctedRadius);

            if (learnedSpeed > maxSpeed)
                return maxSpeed;

            return learnedSpeed;
        }


        private void CalculateAcceleration() {
            accDistance += getDeltaDistance(LastPosition == null ? null : LastPosition.PiecePosition, CurrentPosition.PiecePosition);
            ++accTicks;

            if (accDistance > 10.0f) {
                Acceleration = (Speed - LastSpeed) / accTicks;

                //Console.WriteLine("speed1 {0}, speed2 {1}, acc {2}", Speed, Speed2, acc);

                accDistance -= 10.0f;
                accTicks = 0;
            }

            //Console.WriteLine("Acceleration {0}", Acceleration);
        }

        public SwitchLaneMessage.Direction Lane() {
            int currentlySwitching = CurrentPosition.PiecePosition.Lane.StartLaneIndex - CurrentPosition.PiecePosition.Lane.EndLaneIndex;

            if (currentlySwitching != 0)
                return SwitchLaneMessage.Direction.Same;

            float weightedAngle = 0f;
            uint anglePieceCount = 0;

            for (int indexAccumulator = 0; indexAccumulator < CurrentRace.Track.Pieces.Count; ++indexAccumulator) {
                int pieceIndex = (CurrentPosition.PiecePosition.PieceIndex + indexAccumulator + 1) % CurrentRace.Track.Pieces.Count;
                Piece piece = CurrentRace.Track.Pieces[pieceIndex];

                bool isAnglePiece = IsAnglePiece(piece);

                if (piece.Switch && anglePieceCount > 0)
                    break;

                if (isAnglePiece && !piece.Switch) {
                    ++anglePieceCount;
                    weightedAngle += piece.Angle;
                }
            }

            if (anglePieceCount == 0)
                return SwitchLaneMessage.Direction.Same;

            weightedAngle /= (float)anglePieceCount;

            if (Math.Abs(weightedAngle) < 1.0f)
                return SwitchLaneMessage.Direction.Same; // No need to switch lanes yet.

            SwitchLaneMessage.Direction direction;
            if (weightedAngle > 0f)
                direction = SwitchLaneMessage.Direction.Right;
            else
                direction = SwitchLaneMessage.Direction.Left;

            int lastLaneIndex = CurrentRace.Track.Lanes.Count - 1;

            //Console.WriteLine("Lane: {0} {1} {2}", weightedAngle, CurrentPosition.PiecePosition.Lane.StartLaneIndex, CurrentPosition.PiecePosition.Lane.EndLaneIndex);

            if (CurrentPosition.PiecePosition.Lane.EndLaneIndex == 0 && direction == SwitchLaneMessage.Direction.Left) // Car at the leftmost lane
                return SwitchLaneMessage.Direction.Same;
            else if (CurrentPosition.PiecePosition.Lane.EndLaneIndex == lastLaneIndex && direction == SwitchLaneMessage.Direction.Right) // Car at the rightmost lane
                return SwitchLaneMessage.Direction.Same;

            // Here we want to switch lane before next switch.

            return direction;
        }

        internal void UpdateRaceStatus(List<CarPosition> currentPositions) {
            if (opponents == null) {
                opponents = new Dictionary<string, OpponentData>();
                foreach (var car in currentPositions) {
                    if (car.Id.Color != MyCar.Color)
                        opponents[car.Id.Color] = new OpponentData(car);
                }               
            }

            var i = 0;

            CarPositions = currentPositions;
            foreach (var car in currentPositions) {
                if (car.Id.Color == MyCar.Color) {
                    LastPosition = CurrentPosition;
                    CurrentPosition = car;
                    CurrentPiece = CurrentRace.Track.Pieces[CurrentPosition.PiecePosition.PieceIndex];

                    CalculateSpeed();
                    CalculateAcceleration();
                    //CalculateSlipConstants();

                    if (LastPosition != null)
                        updateCorners(LastPosition, CurrentPosition, Speed);

                    //printStuff();
                } else {
                    var opponent = opponents[car.Id.Color];

                    opponent.LastPosition = opponent.CurrentPosition;
                    opponent.LastSpeed = opponent.Speed;
                    opponent.CurrentPosition = car;
                    opponent.Speed = getDeltaDistance(opponent.LastPosition == null ? null : opponent.LastPosition.PiecePosition, opponent.CurrentPosition.PiecePosition);
                    // updateCorners(opponent.LastPosition, opponent.CurrentPosition, opponent.Speed);
                }
            }

            foreach (var opponent in opponents.Values) {
                opponent.LastDistance = opponent.DistanceToMe;
                opponent.DistanceToMe += Speed - opponent.Speed;
            }

            var no = getNearestOpponent();

            if (no != NearestOpponent && no != null) {
                Console.WriteLine("Seuraavaks pitäs ohittaaaa: " + no.Id.Name);
                NearestOpponent = no;
            }
        }

        private void updateCorners(CarPosition lastPosition, CarPosition currentPosition, float speed)
        {
            if (lastPosition == null)
                return;

            var lastIndex = lastPosition.PiecePosition.PieceIndex;
            var currentIndex = currentPosition.PiecePosition.PieceIndex;

            var carEnteredCorner = lastIndex != currentIndex && Corners.ContainsKey(currentIndex) && (!Corners.ContainsKey(lastIndex) || Corners[currentIndex] != Corners[lastIndex]);
            if (carEnteredCorner) {
                var laneIndex = currentPosition.PiecePosition.Lane.EndLaneIndex;
                Corners[currentIndex].Lanes[laneIndex].EntrySpeeds.Add(Speed);
            }

            var carExitedCorner = lastIndex != currentIndex && Corners.ContainsKey(lastIndex) && (!Corners.ContainsKey(currentIndex) || Corners[currentIndex] != Corners[lastIndex]);
            if (carExitedCorner) {

                var laneIndex = lastPosition.PiecePosition.Lane.EndLaneIndex;
                if (Corners[lastIndex].Lanes[laneIndex].Exits.Count < 7) {
                    Corners[lastIndex].Lanes[laneIndex].Exits.Add(new CornerExit() {
                        Angle = Math.Abs(currentPosition.Angle),
                        Speed = speed
                    });
                }

                Console.WriteLine("CAR EXIT CORNER {0}: s:{1} angle:{2}", Corners[lastIndex].StartIndex, speed, currentPosition.Angle);
            }

        }

        public class LaneChoice
        {
            public float Length = 0f;
            public SwitchLaneMessage.Direction Direction = SwitchLaneMessage.Direction.Same;
            public int LaneIndex;
        }

        int longestStraightPiece = -1;
        internal BaseMessage GetResponseMessage() {
            if (longestStraightPiece == -1) {
                longestStraightPiece = getLongestStraightIndex();
            }

            int nextPieceIndex = (CurrentPosition.PiecePosition.PieceIndex + 1) % CurrentRace.Track.Pieces.Count;            
            Piece nextPiece = CurrentRace.Track.Pieces[nextPieceIndex];
            var pieceLength = getPieceLength(CurrentPiece, CurrentPosition.PiecePosition);
            if (nextPiece.Switch && pieceLength - CurrentPosition.PiecePosition.InPieceDistance < Speed && !SwitchRequested) {
                
                List<LaneChoice> laneChoices = Lanes();
                laneChoices = laneChoices.OrderBy(o => o.Length).ToList();

                var direction = laneChoices[0].Direction;
                if (NearestOpponent != null && NearestOpponent.CurrentPosition.PiecePosition.Lane.EndLaneIndex == laneChoices[0].LaneIndex && DistanceToNearestOpponent < 10f)
                    direction = laneChoices[1].Direction;
                else
                    direction = laneChoices[0].Direction;

                if (direction != SwitchLaneMessage.Direction.Same)
                {
                    SwitchRequested = true;
                    return new SwitchLaneMessage(direction);
                }
            } else if (!nextPiece.Switch && SwitchRequested) {
                SwitchRequested = false;
            }

            if (!IsAnglePiece(CurrentPiece) && TurboAvailable && getDistanceBeforeCorner() > 300f)
            {
                Turbo = null;
                return new TurboMessage();
            }

            return new ThrottleMessage(GetThrottle(), GameTick);
        }

        private List<LaneChoice> Lanes()
        {
            var startSwitchIndex = CurrentPosition.PiecePosition.PieceIndex + 1;

            var lanes = new List<LaneChoice>();

            for (var laneIndex = 0; laneIndex < CurrentRace.Track.Lanes.Count; ++laneIndex)
            {
                var lane = CurrentRace.Track.Lanes[laneIndex];
                var currentLaneIndex = CurrentPosition.PiecePosition.Lane.EndLaneIndex;
                if (Math.Abs(laneIndex - currentLaneIndex) <= 1)
                {
                    var lc = new LaneChoice {LaneIndex = laneIndex};
                    if (laneIndex < currentLaneIndex)
                        lc.Direction = SwitchLaneMessage.Direction.Left;
                    else if (laneIndex > currentLaneIndex)
                        lc.Direction = SwitchLaneMessage.Direction.Right;
                    else
                        lc.Direction = SwitchLaneMessage.Direction.Same;

                    lanes.Add(lc);
                }
            }

            for (int i = 0; i < CurrentRace.Track.Pieces.Count; ++i)
            {
                int currentIndex = (i + startSwitchIndex)%CurrentRace.Track.Pieces.Count;
                Piece p = CurrentRace.Track.Pieces[currentIndex];

                if (p.Switch && currentIndex != startSwitchIndex) {
                    break;
                }

                foreach (var laneChoice in lanes)
                {
                    var piecePosition = new PiecePosition()
                    {
                        Lane =
                            new LanePosition()
                            {
                                EndLaneIndex = laneChoice.LaneIndex,
                                StartLaneIndex = laneChoice.LaneIndex
                            }
                    };

                    laneChoice.Length += getPieceLength(p, piecePosition);

                    if (CurrentPosition.PiecePosition.Lane.EndLaneIndex != laneChoice.LaneIndex)
                        laneChoice.Length += 1f;

                }
            }

            return lanes;
        }

        private bool startLog = false;
        private int laps = 0;

        private void printStuff() {
            Lane lane = CurrentRace.Track.Lanes[CurrentPosition.PiecePosition.Lane.StartLaneIndex];

            float pieceLength = getPieceLength(CurrentPiece, CurrentPosition.PiecePosition);
            float correctRadius = getCorrectedRadius(CurrentPiece, CurrentPosition.PiecePosition);
            
            float deltaDistance = getDeltaDistance(LastPosition == null ? null : LastPosition.PiecePosition, CurrentPosition.PiecePosition);
            float deltaCarAngle = getDeltaCarAngle(LastPosition, CurrentPosition);

            /*
            float slipAngle = CurrentPosition.Angle;
            
            float deltaAngle = (deltaDistance * 360f) / ((float)Math.PI * 2f * corectRadius);

            float accTan = corectRadius * (deltaDistance - lastDeltaDistance);
            float accNorm = (deltaDistance * deltaDistance) / corectRadius;

            double acc = Math.Sqrt(accTan * accTan + accNorm * accNorm);

            double angle = Math.Atan(accNorm / accTan);
            */

            if (LastPosition != null && LastPosition.PiecePosition.PieceIndex != CurrentPosition.PiecePosition.PieceIndex && CurrentPosition.PiecePosition.PieceIndex == 0)
                ++laps;

            startLog |= /*CurrentPiece.Radius > 0.1f && */laps == 0;

            //if (CurrentPosition.PiecePosition.PieceIndex > (CurrentRace.Track.Pieces.Count - 10))
            if (startLog)
                LogCsv(CurrentPosition.PiecePosition.PieceIndex, correctRadius, CurrentPosition.Angle, deltaCarAngle, deltaCarAngle - lastDeltaCarAngle, deltaDistance);
            
            lastDeltaDistance = deltaDistance;
            lastDeltaCarAngle = deltaCarAngle;
        }

        #region HelperMethods

        private OpponentData getNearestOpponent() {
            OpponentData nearest = null;
            float nearestDistance = 9999999f;
            foreach (var opponent in opponents.Values) {
                var distance = distanceBetween(CurrentPosition.PiecePosition, opponent.CurrentPosition.PiecePosition) - CurrentRace.Cars[0].Dimensions.Length;
                if (nearest == null || distance < nearestDistance) {
                    nearest = opponent;
                    nearestDistance = distance;
                }
            }

            DistanceToNearestOpponent = nearestDistance;
            return nearest;
        }

        /**
         * laskee etäisyyden jostain pisteestä toiseen, palauttaa 9999 jos on samalla palalla muttei edellä
         */
        private float distanceBetween(PiecePosition from, PiecePosition to) {
            if (from.PieceIndex == to.PieceIndex && from.InPieceDistance < to.InPieceDistance) {
                return to.InPieceDistance - from.InPieceDistance;
            } else if (from.PieceIndex == to.PieceIndex && from.InPieceDistance > to.InPieceDistance) {
                return 9999f;
            } else {
                var f = getPieceDistance(to.PieceIndex, from) + to.InPieceDistance;
                return f;
            }            
        }

        private int getNextCornerPiece() {
            for (var i = 0; i < CurrentRace.Track.Pieces.Count; ++i) {
                var nextPieceIndex = (CurrentPosition.PiecePosition.PieceIndex + 1 + i) % CurrentRace.Track.Pieces.Count;

                if (IsAnglePiece(CurrentRace.Track.Pieces[nextPieceIndex])) {
                    return nextPieceIndex;
                }
            }

            return -1;
        }

        private float getDistanceBeforeCorner() {
            var cornerIndex = getNextCornerPiece();

            var distance = getPieceDistance(cornerIndex, CurrentPosition.PiecePosition);

            return distance;
        }

        private int getLongestStraightIndex() {
            var startPiece = -1;
            var length = 0f;
            var longest = -1;
            var currentLength = 0f;

            var over = false;

            for (int i = 0; i < CurrentRace.Track.Pieces.Count || !over; ++i) {
                int pieceIndex = i  % CurrentRace.Track.Pieces.Count;

                var piece = CurrentRace.Track.Pieces[pieceIndex];
               
                if (IsAnglePiece(piece)) {
                    if (currentLength > length) {
                        longest = startPiece;
                        length = currentLength;
                    }
                    currentLength = 0f;
                    startPiece = -1;

                    if (i > CurrentRace.Track.Pieces.Count) {
                        over = true;
                    }

                } else {
                    if (startPiece == -1)
                        startPiece = i;

                    currentLength += piece.Length;
                }
            }

            Console.WriteLine("longest straight: {0}", longest);
            return longest;
        }

        private float getPieceLength(Piece piece, PiecePosition piecePosition) {
            float pieceLength = piece.Length;

            if (pieceLength < 0.1f) {
                float correctedRadius = getCorrectedRadius(piece, piecePosition);
                pieceLength = Math.Abs(piece.Angle) * ((float)Math.PI / 180f) * correctedRadius;
            }

            if (piece.Switch && piecePosition.Lane.StartLaneIndex != piecePosition.Lane.EndLaneIndex) {
                Lane startLane = CurrentRace.Track.Lanes[piecePosition.Lane.StartLaneIndex];
                Lane endLane = CurrentRace.Track.Lanes[piecePosition.Lane.EndLaneIndex];

                pieceLength = (float)Math.Sqrt(Math.Pow(Math.Abs((double)startLane.DistanceFromCenter - (double)endLane.DistanceFromCenter), 2.0) + Math.Pow((double)pieceLength, 2.0));
            }

            return pieceLength;
        }

        private float getCorrectedRadius(Piece piece, PiecePosition piecePosition) {
            if (piece.Length > 0.1f)
                return 0f;

            Lane lane = CurrentRace.Track.Lanes[piecePosition.Lane.EndLaneIndex];

            float pieceDistanceFromCenter = lane.DistanceFromCenter;
            if (piece.Angle > 0f)
                pieceDistanceFromCenter *= -1;

            return piece.Radius + pieceDistanceFromCenter;
        }

        private float getDeltaDistance(PiecePosition lastPosition, PiecePosition currentPosition) {
            float deltaDistance;
            if (lastPosition == null) {
                deltaDistance = currentPosition.InPieceDistance;
            } else if (lastPosition.PieceIndex == currentPosition.PieceIndex) {
                deltaDistance = currentPosition.InPieceDistance - lastPosition.InPieceDistance;
            } else {
                Piece lastPiece = CurrentRace.Track.Pieces[lastPosition.PieceIndex];
                float lastPieceLength = getPieceLength(lastPiece, lastPosition);

                deltaDistance = (lastPieceLength - lastPosition.InPieceDistance) + currentPosition.InPieceDistance;
            }

            return deltaDistance;
        }

        private float getDeltaCarAngle(CarPosition lastPosition, CarPosition currentPosition) {
            float deltaAngle;
            if (lastPosition == null)
                deltaAngle = currentPosition.Angle;
            else
                deltaAngle = currentPosition.Angle - lastPosition.Angle;

            return deltaAngle;
        }

        private bool IsAnglePiece(Piece piece) {
            return Math.Abs(piece.Angle) > 1f;
        }

        private void LogCsv(params object[] args) {
            string message = "";
            foreach (var arg in args) {
                message += arg + ";";
            }
            message += "\n";
#if DEBUG
            //File.AppendAllText("c:/tmp/AILog.csv", message);
            //File.AppendAllText("c:/Users/Janne/AILog.csv", message);
#endif
        }

        private float getPieceDistance(int toPieceIndex, PiecePosition fromPosition) {
            float totalLength = getPieceLength(CurrentRace.Track.Pieces[fromPosition.PieceIndex], fromPosition) - fromPosition.InPieceDistance;

            int index = fromPosition.PieceIndex;
            int i = -5;
            while (i != toPieceIndex) {
                i = (index + 1) % CurrentRace.Track.Pieces.Count;
                if (i == toPieceIndex)
                    break;

                totalLength += getPieceLength(CurrentRace.Track.Pieces[i], fromPosition);
                index++;
            }

             return totalLength;
        }

        /// <summary>
        /// Olettaa maksimikiihdytyksen ja maksimijarrutuksen
        /// </summary>
        /// <param name="startVelocity"></param>
        /// <param name="endVelocity"></param>
        /// <returns></returns>
        private int getVelocityChangeTicks(float startVelocity, float endVelocity) {
            int ticks = 0;

            if (startVelocity > endVelocity) { // Deceleration
                float v = startVelocity;

                while ((v -= -throttleK * v) >= endVelocity)
                    ++ticks;
            } else if (startVelocity < endVelocity) {
                float v = startVelocity;

                while ((v += throttleA - (-throttleK * v)) <= endVelocity)
                    ++ticks;
            }

            return ticks;
        }

        /// <summary>
        /// Olettaa maksimikiihdytyksen ja maksimijarrutuksen
        /// </summary>
        /// <param name="startVelocity"></param>
        /// <param name="endVelocity"></param>
        /// <returns></returns>
        private float getVelocityChangeDistance(float startVelocity, float endVelocity) {
            float distance = startVelocity;

            if (startVelocity > endVelocity) { // Deceleration
                float v = startVelocity;

                while ((v -= -throttleK * v) >= endVelocity)
                    distance += v;
            } else if (startVelocity < endVelocity) {
                float v = startVelocity;

                while ((v += throttleA - (-throttleK * v)) <= endVelocity)
                    distance += v;
            }

            return distance;
        }

        #endregion


        #region Parameter calculations

        private bool SlipConstantsCalculated = false;
        private bool ThrottleConstantsCalculated = false;
        private long ThrottleConstantsTickCounter = 0;
        private List<float> kValues = new List<float>();
        private List<float> speeds = new List<float>();

        public float CalculateThrottleConstants() {
            if (LastPosition == null || Speed < 0.01f)
                return 1f;

            ++ThrottleConstantsTickCounter;

            if (ThrottleConstantsTickCounter < CALCULATION_TICK_COUNT) { // kiihdytystä
                speeds.Add(Speed);
                return 1f;
            } else if (ThrottleConstantsTickCounter == CALCULATION_TICK_COUNT) { // yks tikki tyhjää
                return 0f;
            } else { // jarrutusta
                kValues.Add((Speed - LastSpeed) / Speed);

                if (kValues.Count == CALCULATION_TICK_COUNT) {
                    throttleK = kValues.Average();

                    var accelerationValues = new List<float>(speeds.Count);
                    for (int i = 1; i < speeds.Count; ++i)
                        accelerationValues.Add((speeds[i] - speeds[i - 1]) - (throttleK * speeds[i]));

                    throttleA = accelerationValues.Average();

                    maxSpeed = Math.Abs(throttleA / throttleK);

                    ThrottleConstantsCalculated = true;

                    Console.WriteLine("Throttle a:{0}, k:{1}, Vmax:{2}", throttleA, throttleK, maxSpeed);

                    return GetThrottle(); // Calculate throttle again.
                } else {
                    return 0f;
                }
            }
        }

        private List<SlipData> slipDatas = new List<SlipData>();

        public void CalculateSlipConstants() {
            if (SlipConstantsCalculated || CurrentPiece.Radius > 0f || SlipAngleVelocity >= 0f)
                return;

            slipDatas.Add(new SlipData() {
                SlipAngleAcceleration = SlipAngleVelocity - LastSlipAngleVelocity,
                SlipAngleVelocity = SlipAngleVelocity,
                SlipAngle = CurrentPosition.Angle
            });

            if (slipDatas.Count == (CALCULATION_TICK_COUNT << 3)) {
                slipDatas.Sort((slipDataA, slipDataB) => slipDataA.SlipAngleVelocity.CompareTo(slipDataB.SlipAngleVelocity));

                int sampleCount = slipDatas.Count >> 1;

                double k = 0f;
                double c = 0f;

                for (int i = 0; i < sampleCount; ) {
                    SlipData slipDataA = slipDatas.ElementAt(i++);
                    SlipData slipDataB = slipDatas.ElementAt(i++);
                    /*
                    Console.WriteLine("A {0} {1} {2}", slipDataA.SlipAngle, slipDataA.SlipAngleVelocity, slipDataA.SlipAngleAcceleration);
                    Console.WriteLine("B {0} {1} {2}", slipDataB.SlipAngle, slipDataB.SlipAngleVelocity, slipDataB.SlipAngleAcceleration);

                    Console.WriteLine("k{0} c{1}", (slipDataB.SlipAngleAcceleration * slipDataA.SlipAngleVelocity - slipDataA.SlipAngleAcceleration * slipDataB.SlipAngleVelocity) /
                            (slipDataB.SlipAngleVelocity * slipDataA.SlipAngle - slipDataA.SlipAngleVelocity * slipDataB.SlipAngle),
                            (slipDataB.SlipAngleAcceleration * slipDataA.SlipAngle - slipDataA.SlipAngleAcceleration * slipDataB.SlipAngle) /
                            (slipDataB.SlipAngle * slipDataA.SlipAngleVelocity - slipDataA.SlipAngle * slipDataB.SlipAngleVelocity));
                    
                    k +=    (slipDataB.SlipAngleAcceleration * slipDataA.SlipAngleVelocity - slipDataA.SlipAngleAcceleration * slipDataB.SlipAngleVelocity) /
                            (slipDataB.SlipAngleVelocity * slipDataA.SlipAngle - slipDataA.SlipAngleVelocity * slipDataB.SlipAngle);
                    */
                    /*
                    double lC = (slipDataB.SlipAngleAcceleration * slipDataA.SlipAngle - slipDataA.SlipAngleAcceleration * slipDataB.SlipAngle) /
                                (slipDataB.SlipAngle * slipDataA.SlipAngleVelocity - slipDataA.SlipAngle * slipDataB.SlipAngleVelocity);
                    */

                    double lC = 0.1;
                    c += lC;

                    k += -(10f*slipDataA.SlipAngleAcceleration + slipDataA.SlipAngleVelocity) / (10f*slipDataA.SlipAngle);

                    Console.WriteLine("k {0}", -(10f * slipDataA.SlipAngleAcceleration + slipDataA.SlipAngleVelocity) / (10f * slipDataA.SlipAngle));
                    /*
                    Console.WriteLine("k{0} k{1}", (slipDataB.SlipAngleAcceleration * slipDataA.SlipAngleVelocity - slipDataA.SlipAngleAcceleration * slipDataB.SlipAngleVelocity) /
                            (slipDataB.SlipAngleVelocity * slipDataA.SlipAngle - slipDataA.SlipAngleVelocity * slipDataB.SlipAngle),
                            -(10f*slipDataA.SlipAngleAcceleration + slipDataA.SlipAngleVelocity) / (10f*slipDataA.SlipAngle));
                    */
                }

                k /= (double)(sampleCount >> 1);
                c /= (double)(sampleCount >> 1);

                Console.WriteLine("Acceleration k:{0}, c:{1}", k, c);

                SlipConstantsCalculated = true;
            }
        }

        private struct SlipData {
            public float SlipAngleAcceleration;
            public float SlipAngleVelocity;
            public float SlipAngle;
        }

        #endregion
    }
}
