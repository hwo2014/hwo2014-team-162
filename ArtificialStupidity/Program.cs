﻿using ArtificialStupidity.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArtificialStupidity {
    class Program {
        static void Main(string[] args) {

            Settings.MAP = Tracks.GERMANY;

            if (args.Length > 3) {
                Settings.HOST = args[0];
                Settings.PORT = int.Parse(args[1]);
                Settings.BOTNAME = args[2];
                Settings.BOTKEY = args[3];
                Settings.MAP = Tracks.JOIN;
            }

            // Käynnistetää servu
            if (args.Length == 2) {
                SimpleCarBot bot = new SimpleCarBot();
                Settings.MAP = args[1];
                bot.startMany(int.Parse(args[0]));
            } else {
                SimpleCarBot bot = new SimpleCarBot();
                bot.Start();
            }
        }
    }
}
