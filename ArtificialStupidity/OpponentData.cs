﻿using ArtificialStupidity.Messages.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity {
    class OpponentData {
        public CarId Id;
        public CarPosition CurrentPosition;
        public CarPosition LastPosition;

        public float Speed;
        public float LastSpeed;

        public float DistanceToMe;
        public float LastDistance;

        public OpponentData(CarPosition car) {
            this.Id = car.Id;
            Speed = 0;
            LastSpeed = 0;
            LastDistance = 0;
            DistanceToMe = 0;
        }
    }
}
