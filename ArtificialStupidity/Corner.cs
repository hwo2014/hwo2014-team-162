﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtificialStupidity {
    class Corner
    {
        public int StartIndex;
        public int EndIndex;

        public Dictionary<int, CornerLane> Lanes = new Dictionary<int, CornerLane>();

        public float BaseRadius { get; set; }

        public float BaseAngle { get; set; }
    }

    public class CornerLane
    {
        public int LaneIndex;
        public float Radius;
        public float Length;
        public List<float> EntrySpeeds = new List<float>();
        public List<CornerExit> Exits = new List<CornerExit>();
        public List<CornerPass> Passes = new List<CornerPass>();
    }

    public class CornerExit
    {
        public float Speed;
        public float Angle;
    }

    public class CornerPass
    {
        public float Speed;
        public float Angle;
    }
}
