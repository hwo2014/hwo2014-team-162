﻿using ArtificialStupidity.Messages;
using ArtificialStupidity.Messages.Client;
using ArtificialStupidity.Messages.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ArtificialStupidity {
    class SimpleCarBot {

        public RaceData RaceData;

        public void Start() {
            if (Settings.MAP == Tracks.JOIN)
                RaceData = null;
            else
                RaceData = new RaceData(Settings.MAP) { CarCount = 1 };

            BotServer botServer = createNewBotserver();

            if (RaceData == null) {
                botServer.Start(new JoinMessage(Settings.BOTKEY, Settings.BOTNAME));
            } else {
                botServer.Start(new JoinRaceMessage() { Data = RaceData });
            }

            

#if DEBUG
            Console.WriteLine("\n\n press enter");
            Console.In.ReadLine();
#endif
        }

        private BotServer createNewBotserver() {
            var botServer = new BotServer();
            RaceMessageHandler raceHandler = new RaceMessageHandler();
            botServer.GotMessage += (message) => {
                var reaction = raceHandler.React(message);

                if (reaction == null) {
                    botServer.Stop();
                    return;
                }
                if (reaction is EmptyMessage) {

                } else {
                    botServer.Send(reaction);
                }
            };

            return botServer;
        }


        public void startMany(int count) {

            for (int i = 0; i < count; ++i) {

                if (i == 0) {
                    ThreadPool.QueueUserWorkItem((e) => {
                        BotServer botServer = createNewBotserver();
                        var botId = new Join() { Key = Settings.BOTKEY, Name = "botti " + i };
                        botServer.Start(new CreateRaceMessage() { Data = new RaceData(Settings.MAP) { BotId = botId, CarCount = count + 1 } });
                    });
                } else {
                    ThreadPool.QueueUserWorkItem((e) => {
                        BotServer botServer = createNewBotserver();
                        var botId = new Join() { Key = Settings.BOTKEY, Name = "botti " + i };
                        botServer.Start(new JoinRaceMessage() { Data = new RaceData(Settings.MAP) { BotId = botId, CarCount = count + 1 } });
                    });
                }

                Thread.Sleep(500);
            }

            while (true) {
                Thread.Sleep(1000);
            }
        }
    }
}
